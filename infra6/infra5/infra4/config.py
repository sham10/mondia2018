from . import mongo

REDIS_HOST = 'localhost'


CONFIG_VERSION = 'V1'


def _get_collection():
    db = mongo.get_connection('lownofell')
    return db.config


def _get_version_config(version):
    coll = _get_collection()
    config = coll.find_one({'_id': version})

    return config


def get_current_config():
    return _get_version_config(CONFIG_VERSION) or {}


def get_config(key, default=None):
    config = get_current_config()

    return config.get(key, default)


def set_config(key, value):
    config = get_current_config()
    config[key] = value

    coll = _get_collection()
    coll.update({'_id': CONFIG_VERSION}, {'$set': config}, upsert=True)
