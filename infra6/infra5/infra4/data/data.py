import os
import sqlite3

here, _ = os.path.split(__file__)
here = os.path.abspath(here)

_seq_lite_path = os.path.join(here, 'id_sequence.db')

_seq_lite_conn = None


def allocate_id():
    global _seq_lite_conn

    if not _seq_lite_conn:
        _seq_lite_conn = sqlite3.connect(_seq_lite_path)

    cursor = _seq_lite_conn.cursor()
    try:
        cursor.execute('UPDATE id_generator SET _id = _id + 1')
        res = cursor.execute('SELECT _id FROM id_generator')
        new_id = next(res)
    except:
        raise
    else:
        _seq_lite_conn.isolation_level = None
        _seq_lite_conn.execute('VACUUM')
        _seq_lite_conn.commit()

    return new_id[0]
