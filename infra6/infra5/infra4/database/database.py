insert_tpl = 'INSERT INTO {table} ({fields}) VALUES ({values});'


def load_data(cursor, table_name, fields, sequence):
    insert_stat = insert_tpl.format(table=table_name,
                                    fields=', '.join(fields),
                                    values=', '.join('?' * len(fields)))
    cursor.executemany(insert_stat, sequence)
