import os
import sqlite3

here, _ = os.path.split(__file__)
here = os.path.abspath(here)


_endpoints = {
    'scores': os.path.join(here, 'scores.db')
}


def get_endpoint_conn(endpoint):
    endpoint_path = _endpoints.get(endpoint)
    if not endpoint_path:
        raise Exception('unknown endpoint "{}"'.format(endpoint))

    with sqlite3.connect(endpoint_path) as conn:
        return conn
