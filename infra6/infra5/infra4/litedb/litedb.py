import os
import sqlite3

here, _ = os.path.split(__file__)
here = os.path.abspath(here)


def get_litedb_conn():
    litedb_path = os.path.join(here, 'tip.db')
    with sqlite3.connect(litedb_path) as conn:
        return conn
