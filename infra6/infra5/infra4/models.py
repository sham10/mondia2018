import datetime
import json
import random

from . import mongo
from . import constants

_SKIP = object()


class Property(object):
    def get_value_for_datastore(self, instance):
        return getattr(instance, self.attr_name, _SKIP)

    def configure(self, name):
        self.name = name
        self.attr_name = '_' + name

    def convert(self, value):
        return value

    def validate(self, value):
        data_type = getattr(self, '_value_type', None)

        if data_type and value is not None and not isinstance(value, data_type):
            raise ValueError('"%s" invalid value type %s, expected %s' % (self.name, type(value), data_type))

    def load(self, value):
        return value

    def save(self, value):
        return value

    def __init__(self, repeated=False):
        self._repeated = repeated

    def __get__(self, instance, owner):
        return getattr(instance, self.attr_name, None)

    def __set__(self, instance, value):
        if self._repeated:
            if value is None:
                value = []
            if not isinstance(value, (list, tuple)):
                raise Exception('%s value must be instance of list or tuple %s' % (self.name, type(value)))

            value = [self.convert(cur_value) for cur_value in value]

            [self.validate(cur_value) for cur_value in value]
        else:
            value = self.convert(value)
            self.validate(value)

        setattr(instance, self.attr_name, value)


class StringProperty(Property):
    _value_type = str


class BlobProperty(Property):
    def save(self, value):
        return json.dumps(value)

    def load(self, value):
        return json.loads(value)


class IntProperty(Property):
    _value_type = int

    def convert(self, value):
        if isinstance(value, str):
            if value:
                try:
                    return int(value)
                except ValueError:
                    raise ValueError('%s cannot convert %s to int' % (self.name, value))
            return None

        return value


class BoolProperty(Property):
    _value_type = bool


class FloatProperty(Property):
    _value_type = float

    def convert(self, value):
        if isinstance(value, (str, int)):
            return float(value)
        return value


class DateTimeProperty(Property):
    _value_type = datetime.datetime
    _format = constants.DATETIME_FORMAT
    empty = datetime.datetime.max

    def save(self, value):
        return None if value == self.empty else value

    def convert(self, value):
        if isinstance(value, str):
            value = datetime.datetime.strptime(value, self._format)
        return value


class DateProperty(DateTimeProperty):
    _value_type = datetime.datetime
    _format = constants.DATE_FORMAT
    empty = datetime.datetime.max


class ObjectProperty(Property):
    _value_type = dict


class ModelProperty(Property):
    def __init__(self, model, repeated=False):
        super(ModelProperty, self).__init__(repeated)
        self.model_class = model
        self._value_type = model

    def __get__(self, instance, owner):
        result = getattr(instance, self.attr_name, None)
        if not result:
            if self._repeated:
                result = []
            else:
                result = self.model_class()

            setattr(instance, self.attr_name, result)
        return result

    def convert(self, value):
        if isinstance(value, dict):
            return self.model_class(None, **value)
        return value

    def load(self, value):
        return value

    def save(self, value):
        return value.save()


class ModelClass(type):
    def __new__(mcs, name, bases, dct):
        return type.__new__(mcs, name, bases, dct)

    def __init__(cls, name, bases, dct):

        cls._properties = {}

        for base in bases:
            if hasattr(base, '_properties'):
                cls._properties.update(base._properties)

        for key, value in dct.items():
            if isinstance(value, Property):
                value.configure(key)
                cls._properties[key] = value

        super(ModelClass, cls).__init__(name, bases, dct)


class BaseModel(object, metaclass=ModelClass):
    _properties = None

    def update(self, loaded=True, **kwargs):
        for prop_name, prop_value in kwargs.items():
            prop = self._properties.get(prop_name)
            if prop:
                if not loaded and hasattr(prop, 'load'):
                    prop_value = prop.load(prop_value)

                prop.__set__(self, prop_value)

    def __init__(self, loaded=True, **kwargs):
        self.update(loaded, **kwargs)

    def save(self):

        result = {}
        for prop in list(self._properties.values()):
            # value = prop.__get__(self, self.__class__)
            value = prop.get_value_for_datastore(self)
            if value is not _SKIP:
                model_value = None
                if value is not None:
                    if prop._repeated:
                        model_value = []
                        for cur_value in value:
                            cur_value = prop.save(cur_value)
                            model_value.append(cur_value)
                            # if not model_value:
                            #    model_value = _SKIP
                    else:
                        model_value = prop.save(value)

                if model_value is not _SKIP:
                    result[prop.name] = model_value
        return result


class Model(BaseModel):
    _database_ = None
    _collection_ = None

    def __init__(self, identity, loaded=True, **kwargs):
        self._id = identity
        super(Model, self).__init__(loaded, **kwargs)

    @classmethod
    def select(cls, **kwargs):
        db = mongo.get_connection(cls._database_, False)

        collection = getattr(db, cls._collection_)

        for doc in collection.find(kwargs):
            model_id = doc.pop('_id')
            yield cls(model_id, False, **doc)

    @classmethod
    def select_next(cls, entity_id=None):
        db = mongo.get_connection(cls._database_, False)

        collection = getattr(db, cls._collection_)
        count = collection.count()

        while True:
            random_pos = random.randint(0, count - 1)
            doc = collection.find().limit(-1).skip(random_pos).next()
            model_id = doc.pop('_id')
            if model_id != entity_id:
                return cls(model_id, False, **doc)

    @classmethod
    def find(cls, _master=False, **kwargs):
        db = mongo.get_connection(cls._database_, _master)

        collection = getattr(db, cls._collection_)

        doc = collection.find_one(kwargs)
        if doc:
            model_id = doc.pop('_id')
            return cls(model_id, False, **doc)

    @classmethod
    def get(cls, identity, _master=False):
        query = identity if isinstance(identity, dict) else {'_id': identity}
        return cls.find(_master=_master, **query)

    def put(self):
        values = self.save()
        db = mongo.get_connection(self._database_, True)
        collection = getattr(db, self._collection_)
        collection.update({'_id': self._id}, {'$set': values}, upsert=True)


class Entity(Model):
    _database_ = 'mondia2018'

    created = DateTimeProperty()
    modified = DateTimeProperty()
    created_by = IntProperty()
    modified_by = IntProperty()
    deleted = BoolProperty()
    flags = StringProperty(repeated=True)


class User(Entity):
    _collection_ = 'user'

    email = StringProperty()
    first_name = StringProperty()
    last_name = StringProperty()

    facebook = StringProperty()
    google = StringProperty()

    password = StringProperty()
    api_key = StringProperty()
    active = BoolProperty()
    admin = BoolProperty()
    hidden = BoolProperty()

    last_signin = DateTimeProperty()


class Group(Entity):
    _collection_ = 'group'

    name = StringProperty()
    users = IntProperty(repeated=True)
    start_amount = FloatProperty()


class UserACL(Entity):
    _collection_ = 'user_acl'

    user_id = IntProperty()
    group_id = IntProperty()
    admin = BoolProperty()


class UserGroup(Entity):
    _collection_ = 'user_group'

    score = FloatProperty()
    rank = IntProperty()


class BetOption(BaseModel):
    name = StringProperty()
    ratio = FloatProperty()


class Class(Entity):
    _collection_ = 'class'

    name = StringProperty()
    date = DateTimeProperty()

    bet_ids = IntProperty(repeated=True)


class BetApplicationModel(Model):
    bet_id = IntProperty()
    value = StringProperty()


class ClassApplication(Entity):
    _collection_ = 'class_app'

    user_id = IntProperty()
    group_id = IntProperty()
    class_id = IntProperty()
    bets = ModelProperty(BetApplicationModel, repeated=True)
    amount = FloatProperty()
    last_submit_date = DateTimeProperty()


class Bet(Entity):
    _collection_ = 'bet'

    name = StringProperty()
    type = StringProperty()

    date = DateTimeProperty()

    opt_1_name = StringProperty()
    opt_1_ratio = FloatProperty()

    opt_x_name = StringProperty()
    opt_x_ratio = FloatProperty()

    opt_2_name = StringProperty()
    opt_2_ratio = FloatProperty()


class BetApplication(Entity):
    _collection_ = 'bet_app'

    user_id = IntProperty()
    group_id = IntProperty()
    bet_id = IntProperty()
    value = StringProperty()
    amount = FloatProperty()
    submit_date = DateTimeProperty()
