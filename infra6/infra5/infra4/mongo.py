#!/usr/bin/env python2.7

import pymongo

from .constants import DATABASE_NAME

_connections = {}

_host = 'mongodb://mondia2018:qazqaz11@ds119685.mlab.com:19685/mondia2018'


def get_connection(database=DATABASE_NAME, master=True):
    connection = _connections.get(_host)
    if not connection:
        options = {}

        if not master:
            options['read_preference'] = pymongo.ReadPreference.SECONDARY

        connection = _connections[_host] = pymongo.MongoClient(host=_host, **options)
        if master and pymongo.version.startswith('2.'):
            connection.write_concern = {'j': True}
        else:
            connection.read_preference
    return getattr(connection, database)
