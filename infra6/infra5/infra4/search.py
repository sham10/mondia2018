from infra6.infra5.infra4 import mongo


collections_factory = {
    'bets': 'bet',
    'classes': 'class',
    'groups': 'group',
    'class_apps': 'class_app',
}


class ArgsMapper(object):
    def map(self, *args, **kwargs):
        return args[0] if args else kwargs


class RowMapper(object):
    def __call__(self, row):
        result = {}
        for key, value in row.items():
            if key == '_id':
                key = 'id'

            if isinstance(value, list):
                value = ', '.join([str(v) for v in value])

            result[key] = value

        return result


def do_search(entity_type, *args, **kwargs):

    args_mapper = ArgsMapper()
    search_args = args_mapper.map(*args, **kwargs)

    coll_name = collections_factory[entity_type]
    mongo_conn = mongo.get_connection()
    coll = getattr(mongo_conn, coll_name)

    deleted_filter = {'$or': [{'deleted': {'$exists': False}}, {'deleted': False}]}
    query = {'$and': [search_args, deleted_filter]}
    search_res = coll.find(query)

    row_mapper = RowMapper()
    data = [row_mapper(row) for row in search_res]

    return data


def search_one(entity_type, *args, **kwargs):
    result = do_search(entity_type, *args, **kwargs)
    return result and result[0]
