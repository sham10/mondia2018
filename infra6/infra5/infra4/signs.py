#!/usr/bin/env python2.7

import base64
import functools
import hmac
from hashlib import sha256 as sha256

_secret = b'qiyh4XPJGsOZ2MEAyLkfWqeQ'
_hmac = functools.partial(hmac.new, _secret, digestmod=sha256)


def get_signature(*values):
    hash = _hmac()
    [hash.update(value.encode() if isinstance(value, str) else value)
     for value in values]
    return hash.hexdigest().encode()


def encrypt(*values):
    join = b'|'.join
    #value = join(values)
    value = join(str(v).encode() if not isinstance(v, bytes) else v for v in values)
    signature = get_signature(value)

    v = join((value, signature))
    return base64.b64encode(v).strip(b'\r\n=')


def decrypt(token, sep=b'|'):
    try:
        if not isinstance(token, bytes):
            token = str(token).encode()

        modulo = len(token) % 4
        if modulo:
            token += (b'=' * (4 - modulo))
        token = base64.b64decode(token)
        value, hash = token.rsplit(b'|', 1)
        signature = get_signature(value)
        if signature == hash:
            return value.split(sep) if sep else value
    except:
        pass
