#!/usr/bin/env python2.7

import hashlib


def to_dict(func):
    def inner(*args, **kwargs):
        data = func(*args, **kwargs)
        return data and {k: v for k, v in func(*args, **kwargs) if v is not None} or {}
    return inner


def to_list(func):
    def inner(*args):
        return [v for v in func(*args) if v is not None]
    return inner


def get_hash(*args):
    h = hashlib.md5()
    for item in args:
        item = item.encode('utf-8')
        h.update(item)
    return h.hexdigest()
