import collections
import datetime

TODAY = datetime.datetime.combine(datetime.date.today(), datetime.time())
NOW = datetime.datetime.now()

_entity_adapters = collections.defaultdict(list)


def register(entity, *args, **kwargs):
    def _register_entity(cls):
        _entity_adapters[entity].append((cls, args or tuple(), kwargs or {}))
        return cls
    return _register_entity


def get_entity_adapters(entity_type):
    return [adapter(*args, **kwargs) for adapter, args, kwargs in _entity_adapters[entity_type]]


def iter_adapter_attributes(attribute):
    def_value = object()
    used = []
    for _, adapters in _entity_adapters.items():
        for adapter, __, ___ in adapters:
            value = getattr(adapter, attribute, def_value)
            if value and value is not def_value and value not in used:
                used.append(value)
                yield value


def path_get(doc, path):
    keys = path.split('.')
    keys.reverse()
    while keys:
        key = keys.pop()
        if not isinstance(doc, dict):
            return None
        doc = doc.get(key)
    return doc


def path_set(doc, path, value):
    keys = path.split('.')
    keys.reverse()
    dest = doc
    key = keys.pop()
    while keys:
        if key not in dest.keys():
            dest[key] = {}
        dest = dest[key]
        key = keys.pop()
    dest[key] = value
    return doc


def validate_date_range(date, start_date, end_date):
    if date >= start_date:
        return end_date is None or date <= end_date


class BaseAdapter(object):

    TABLE_NAME = None

    segment_properties = []

    def get_segments(self, doc):
        return [doc]

    def is_valid(self, _):
        return True

    def segment_is_valid(self, segment):
        return True

    def get_values(self, segment):
        for prop in self.segment_properties:
            value = path_get(segment, prop)
            if isinstance(value, datetime.datetime) and not value.time():
                value = value.date()
            if isinstance(value, list):
                value = ';'.join([str(v) for v in value])
            yield value


@register('user')
class UsersAdapter(BaseAdapter):
    TABLE_NAME = 'users'

    CREATE_TABLE = '''
        CREATE TABLE users
        (
            id INTEGER PRIMARY KEY,
            first_name TEXT,
            last_name TEXT,
            email TEXT
        )
    '''

    REVISION_DATA = '''
        SELECT
            id,
            first_name,
            last_name
        FROM
            users
        ORDER BY
            id
    '''

    segment_properties = ['entity_id', 'first_name', 'last_name', 'email']


@register('bet')
class BetAdapter(BaseAdapter):
    TABLE_NAME = 'bets'

    CREATE_TABLE = '''
        CREATE TABLE bets
        (
            id INTEGER PRIMARY KEY,
            name TEXT,
            type TEXT,
            date TEXT,
            opt_1_name TEXT,
            opt_1_ratio REAL,
            opt_x_name TEXT,
            opt_x_ratio REAL,
            opt_2_name TEXT,
            opt_2_ratio REAL
        )
    '''

    REVISION_DATA = '''
        SELECT
            id,
            name,
            type,
            date,
            opt_1_name,
            opt_1_ratio,
            opt_x_name,
            opt_x_ratio,
            opt_2_name,
            opt_2_ratio
        FROM
            bets
        ORDER BY
            id
    '''

    segment_properties = ['entity_id', 'name', 'type', 'date', 'opt_1_name',
                          'opt_1_ratio', 'opt_x_name', 'opt_x_ratio', 'opt_2_name', 'opt_2_ratio'
                          ]


@register('class_app')
class ClassAppAdapter(BaseAdapter):
    TABLE_NAME = 'class_apps'

    CREATE_TABLE = '''
        CREATE TABLE class_apps
        (
            id INTEGER PRIMARY KEY,
            user_id INTEGER,
            group_id INTEGER,
            class_id INTEGER,
            amount REAL
        )
    '''

    REVISION_DATA = '''
        SELECT
            id,
            user_id,
            group_id,
            class_id,
            amount
        FROM
            class_apps
        ORDER BY
            id
    '''

    segment_properties = ['entity_id', 'user_id', 'group_id', 'class_id', 'amount']


@register('class_app')
class ClassAppBetsAdapter(BaseAdapter):
    TABLE_NAME = 'class_app_bets'

    CREATE_TABLE = '''
        CREATE TABLE class_app_bets
        (
            class_app_id,
            bet_id INTEGER,
            value STRING
        )
    '''

    REVISION_DATA = '''
        SELECT
            class_app_id,
            bet_id,
            value
        FROM
            class_app_bets
        ORDER BY
            class_app_id,
            bet_id
    '''

    segment_properties = ['entity_id', 'bet_id', 'value']

    def get_segments(self, doc):
        bets = doc.get('bets', [])
        for b in bets:
            yield b
