
#from .adapters import validate_date_range, TODAY, NOW

_entities = {}


def register(cls):
    _entities[cls.ENTITY_NAME] = cls
    return cls


def get_entity(entity_type):
    entity = _entities.get(entity_type)
    if not entity:
        raise ValueError('wrong entity type: %s' % entity_type)
    return entity()


def get_all_entities():
    return _entities.keys()


def get_opt_entities():
    opt_entities = {}
    for name, entity in _entities.items():
        if entity.OPTIMIZATION_PRIORITY:
            opt_entities[entity.OPTIMIZATION_PRIORITY] = entity

    return opt_entities


class Entity(object):
    ENTITY_NAME = None
    MONGO_DB = 'entities'

    ENV = None

    OPTIMIZATION_PRIORITY = None

    opt_indices = []
    optimization_statements = []

    def get_entity_id(self, doc):
        return doc.get('_id')

    def is_valid(self, doc):
        return True

    def optimization_setup(self, db):
        statement = 'CREATE INDEX IF NOT EXISTS {index_name} ON {table} ({column})'

        for item in self.opt_indices:
            table, column = item
            index_name = '_'.join([table, column])
            formatted_statement = statement.format(table=table, column=column, index_name=index_name)
            db.cursor.execute(formatted_statement)

    def optimization_teardown(self, db):
        statement = 'DROP INDEX IF EXISTS {index_name}'

        for item in self.opt_indices:
            table, column = item
            index_name = '_'.join([table, column])
            formatted_statement = statement.format(index_name=index_name)
            db.cursor.execute(formatted_statement)

    def optimize_tables(self, db):
        for statement in self.optimization_statements:
            db.cursor.execute(statement)


@register
class User(Entity):
    ENTITY_NAME = 'user'
    MONGO_COL = 'user'


@register
class Bet(Entity):
    ENTITY_NAME = 'bet'
    MONGO_COL = 'bet'


@register
class ClassApp(Entity):
    ENTITY_NAME = 'class_app'
    MONGO_COL = 'class_app'
