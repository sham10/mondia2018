import logging
import sys
import hashlib
import sqlite3
import tempfile
import shutil
import contextlib
import os
import struct

from infra6.infra5.infra4 import mongo

import entities
import adapters

logger = logging.getLogger(__name__)

PAGE_SIZE = 1000

here, _ = os.path.split(__file__)
here = os.path.abspath(here)

RESULT_PATH = os.path.join(here, '..', '..', 'infra6', 'infra5', 'infra4', 'litedb')


class RevNumGenerator(object):

    def __init__(self):
        self.hash_value = hashlib.md5()

    def add_value(self, *values):
        for v in values:
            if v is None:
                continue

            if isinstance(v, str):
                v = v.encode('utf-8')
            elif isinstance(v, float):
                v = bytearray(struct.pack('f', v))

            self.hash_value.update(bytes(v))

    @property
    def rev_number(self):
        return self.hash_value.hexdigest()


class StatementGenerator(object):

    def __init__(self, connection):
        self._connection = connection
        self._statements = {}

    def _get_table_columns(self, table_name):
        statement = '''PRAGMA table_info({0});'''.format(table_name)
        cursor = self._connection.cursor()
        schema = cursor.execute(statement).fetchall()

        return [column[1] for column in schema]

    def _generate_statement(self, table_name, fields):
        flds = ', '.join(fields)
        values = ', '.join('?' * len(fields))
        return 'REPLACE INTO {0} ({1}) VALUES ({2})'.format(table_name, flds, values)

    def get_statement(self, table_name):
        statement = self._statements.get(table_name)
        if not statement:
            columns = self._get_table_columns(table_name)
            statement = self._generate_statement(table_name, columns)
            self._statements[table_name] = statement
        return statement


class Database(object):

    def __init__(self, db_file):
        if not db_file:
            raise ValueError('no db_file')

        self.db_file = db_file

        self.connection = sqlite3.connect(db_file)
        self.cursor = self.connection.cursor()
        self.statement_generator = StatementGenerator(self.connection)

    def __enter__(self):
        return self

    def __exit__(self, *_):
        self.connection.commit()
        if getattr(self, 'db_file', None):
            self.connection.close()

    def create_tables(self, statements):
        script = ';'.join(statements)
        cur = self.connection.cursor()
        cur.executescript(script)

    def insert(self, table, values):
        statement = self.statement_generator.get_statement(table)
        self.cursor.executemany(statement, values)

    def select(self, statement, *args):
        self.cursor.execute(statement, args)
        for row in self.cursor:
            yield row

    def get_all_identities_from_table(self, table):
        statement = 'SELECT id FROM %s' % table
        for entity_id, in self.select(statement):
            yield str(entity_id)

    def vacuum(self):
        self.connection.execute('VACUUM')


def process_adapter(adapter, doc, entity_id, db):
    segments = adapter.get_segments(doc)
    valid_segments = (segment for segment in segments if adapter.segment_is_valid(segment))

    pendings = []
    for segment in valid_segments:
        if isinstance(segment, dict):
            if 'entity_id' not in segment:
                segment['entity_id'] = entity_id

        segment = adapter.get_values(segment)
        pendings.append(tuple(segment))

    if pendings:
        db.insert(table=adapter.TABLE_NAME, values=pendings)


def process_entity_adapters(entity, doc, entity_id, db):
    for adapter in adapters.get_entity_adapters(entity.ENTITY_NAME):
        if adapter.is_valid(doc):
            process_adapter(adapter, doc, entity_id, db)


def network_env_is_valid(network_modes, env):
    return not env or env in network_modes


def process_entity(entity, db):
    mongo_db = mongo.get_connection()
    collection = getattr(mongo_db, entity.MONGO_COL)

    total_docs = 0

    cursor = collection.find({'deleted': {'$ne': True}})
    for doc in cursor:
        entity_id = entity.get_entity_id(doc)
        if entity.is_valid(doc):
            process_entity_adapters(entity, doc, entity_id, db)

        total_docs += 1

    logger.info('processed %d docs', total_docs)


def process_all_entities(db):
    all_entities = entities.get_all_entities()

    logger.info('starting to process all entities')

    for entity_type in all_entities:
        logger.info('processing entity type: %s', entity_type)
        entity = entities.get_entity(entity_type)

        process_entity(entity, db)


def optimize_all_entities(db):
    opt_entities = entities.get_opt_entities()

    logger.info('starting to optimize all entities')

    for _, entity_class in sorted(opt_entities.items()):
        entity = entity_class()
        logger.info('optimizing %s entity', entity.__class__.__name__)
        entity.optimization_setup(db)
        entity.optimize_tables(db)
        entity.optimization_teardown(db)


def process_globals(db):
    for adapter in adapters.get_entity_adapters('globals'):
        data = adapter.get_global_data()
        segments = adapter.get_segments(data)
        valid_segments = (segment for segment in segments if adapter.segment_is_valid(segment))

        pendings = []
        for segment in valid_segments:
            segment = adapter.get_values(segment)
            pendings.append(tuple(segment))

        if pendings:
            db.insert(table=adapter.TABLE_NAME, values=pendings)


def generate_revision_number(db, revision_statements):
    rev_gen = RevNumGenerator()

    for statement in revision_statements:
        rows = db.select(statement)
        for row in rows:
            rev_gen.add_value(*row)

    return rev_gen.rev_number


def store_revision_number(db, revision_number):
    CREATE = 'CREATE TABLE _sqlite (key TEXT PRIMARY KEY, value TEXT)'
    db.cursor.execute(CREATE)

    INSERT = 'INSERT INTO _sqlite VALUES (?, ?)'
    db.cursor.execute(INSERT, ('revision', revision_number))


def create_litedb(path):
    with Database(path) as db:
        create_statements = adapters.iter_adapter_attributes('CREATE_TABLE')
        db.create_tables(create_statements)

        process_all_entities(db)

        process_globals(db)

        optimize_all_entities(db)

        revision_statements = adapters.iter_adapter_attributes('REVISION_DATA')
        logger.info('generating revision number')
        revision = generate_revision_number(db, revision_statements)
        logger.info('new revision: %s', revision)
        store_revision_number(db, revision)

        db.vacuum()

        return revision


@contextlib.contextmanager
def get_temp_file():
    fd, lite_path = tempfile.mkstemp(prefix='litedb_')
    os.close(fd)
    try:
        yield lite_path
    finally:
        os.unlink(lite_path)


def main():
    with get_temp_file() as lite_path:

        try:
            new_rev = create_litedb(lite_path)
            logger.info('new revision - {}'.format(new_rev))

            file_path = os.path.join(RESULT_PATH, 'tip.db')
            shutil.copy(lite_path, file_path)

        except Exception:
            logger.exception('unhandled exception')
            return 1


if __name__ == '__main__':
    sys.exit(main())
