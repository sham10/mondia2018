import collections
import datetime

TODAY = datetime.datetime.combine(datetime.date.today(), datetime.time())
NOW = datetime.datetime.now()

_source_adapters = collections.defaultdict(list)


def register(source, *args, **kwargs):
    def _register_source(cls):
        _source_adapters[source].append((cls, args or tuple(), kwargs or {}))
        return cls
    return _register_source


def get_source_adapters(source_type):
    return [adapter(*args, **kwargs) for adapter, args, kwargs in _source_adapters[source_type]]


def iter_adapter_attributes(attribute):
    def_value = object()
    used = []
    for _, adapters in _source_adapters.items():
        for adapter, __, ___ in adapters:
            value = getattr(adapter, attribute, def_value)
            if value and value is not def_value and value not in used:
                used.append(value)
                yield value


def path_get(doc, path):
    keys = path.split('.')
    keys.reverse()
    while keys:
        key = keys.pop()
        if not isinstance(doc, dict):
            return None
        doc = doc.get(key)
    return doc


def path_set(doc, path, value):
    keys = path.split('.')
    keys.reverse()
    dest = doc
    key = keys.pop()
    while keys:
        if key not in dest.keys():
            dest[key] = {}
        dest = dest[key]
        key = keys.pop()
    dest[key] = value
    return doc


def validate_date_range(date, start_date, end_date):
    if date >= start_date:
        return end_date is None or date <= end_date


class BaseAdapter(object):

    TABLE_NAME = None

    segment_properties = []

    def get_segments(self, doc, bets_results_by_id):
        return [doc]

    def is_valid(self, _):
        return True

    def segment_is_valid(self, segment):
        return True

    def get_values(self, segment):
        for prop in self.segment_properties:
            value = path_get(segment, prop)
            if isinstance(value, datetime.datetime) and not value.time():
                value = value.date()
            if isinstance(value, list):
                value = ';'.join([str(v) for v in value])
            yield value


@register('class_app')
class ClassAppScoreAdapter(BaseAdapter):
    TABLE_NAME = 'scores'

    CREATE_TABLE = '''
        CREATE TABLE IF NOT EXISTS scores
        (
            user_id INTEGER,
            group_id INTEGER,
            bet_id INTEGER,
            bet_app_id INTEGER,
            class_id INTEGER,
            class_app_id INTEGER,
            score REAL
        )
    '''

    segment_properties = ['user_id', 'group_id', '', '', 'class_id', 'class_app_id', 'score']

    def get_segments(self, doc, bets_results_by_id):
        class_app_id, user_id, group_id, class_id, amount = (doc.get(p) for p in ('_id', 'user_id', 'group_id',
                                                                                  'class_id', 'amount'))
        bets = doc.get('bets', [])

        agg_ratio = 1
        count = 0
        score = 0

        for bet in bets:
            bet_id, value, = bet['bet_id'], bet['value']

            bet_result = bets_results_by_id.get(bet_id)
            if bet_result:
                res_value, res_ratio = bet_result
                if res_value == value:
                    agg_ratio *= res_ratio
                    count += 1

        if count >= 3:
            score = amount * agg_ratio

        yield {
            'user_id': user_id,
            'group_id': group_id,
            'class_id': class_id,
            'class_app_id': class_app_id,
            'score': score or -amount
        }


@register('class_app')
class ClassAppBetsAdapter(BaseAdapter):
    TABLE_NAME = 'bets'

    CREATE_TABLE = '''
        CREATE TABLE IF NOT EXISTS bets
        (
            user_id INTEGER,
            group_id INTEGER,
            class_id INTEGER,
            bet_id INTEGER,
            date TEXT,
            value TEXT
        )
    '''

    segment_properties = ['user_id', 'group_id', 'class_id', 'bet_id', 'date', 'value']

    def get_segments(self, doc, bets_results_by_id):
        user_id, group_id, date, class_id = (doc.get(p) for p in ('user_id', 'group_id', 'date', 'class_id'))

        bets = doc.get('bets', [])

        for bet in bets:
            bet_id, value, = bet['bet_id'], bet['value']

            bet_result = bets_results_by_id.get(bet_id)
            if bet_result:
                res_value, _ = bet_result
                if res_value == value:
                    yield {
                        'user_id': user_id,
                        'group_id': group_id,
                        'class_id': class_id,
                        'bet_id': bet_id,
                        'date': date,
                        'value': value
                    }


@register('bet_app')
class BetAppScoreAdapter(BaseAdapter):
    TABLE_NAME = 'scores'

    CREATE_TABLE = '''
        CREATE TABLE IF NOT EXISTS bets
        (
            user_id INTEGER,
            group_id INTEGER,
            class_id INTEGER,
            bet_id INTEGER,
            date TEXT,
            value TEXT
        )
    '''

    segment_properties = ['user_id', 'group_id', 'bet_id', '', 'score']

    def get_segments(self, doc, bets_results_by_id):
        bet_app_id, user_id, group_id, bet_id, value, amount, date = (doc.get(p) for p in ('_id', 'user_id', 'group_id',
                                                                                           'bet_id', 'value', 'amount',
                                                                                           'submit_date'))

        score = 0
        bet_result = bets_results_by_id.get(bet_id)
        if bet_result:
            res_value, res_ratio = bet_result
            if res_value == value:
                score = amount * res_ratio

        yield {
            'user_id': user_id,
            'group_id': group_id,
            'bet_id': bet_id,
            'bet_app_id': bet_app_id,
            'date': date,
            'score': score or -amount
        }


@register('group')
class GroupStartAmountAdapter(BaseAdapter):
    TABLE_NAME = 'users_start_amount'

    CREATE_TABLE = '''
        CREATE TABLE IF NOT EXISTS users_start_amount
        (
            group_id INTEGER,
            user_id INTEGER,
            start_amount REAL
        )
    '''

    segment_properties = ['source_id', 'user_id', 'start_amount']

    def get_segments(self, doc, _):
        users, start_amount = doc.get('users', []), doc.get('start_amount')

        for user_id in users:
            yield {
                'user_id': user_id,
                'start_amount': start_amount
            }
