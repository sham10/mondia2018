import sys
import argparse
import json
import datetime
import logging
from jsonschema import (
    validate,
    ValidationError
)

from infra6.infra5.infra4 import (
    mongo,
    search
)
from infra6.infra5.infra4.database.endpoints import get_endpoint_conn
from infra6.infra5.infra4.exceptions import ValidationError as LocalValidationError

import sources
import adapters


logger = logging.getLogger(__name__)

NOW = datetime.datetime.now()


class StatementGenerator(object):

    def __init__(self, connection):
        self._connection = connection
        self._statements = {}

    def _get_table_columns(self, table_name):
        statement = '''PRAGMA table_info({0});'''.format(table_name)
        cursor = self._connection.cursor()
        schema = cursor.execute(statement).fetchall()

        return [column[1] for column in schema]

    def _generate_statement(self, table_name, fields):
        flds = ', '.join(fields)
        values = ', '.join('?' * len(fields))
        return 'REPLACE INTO {0} ({1}) VALUES ({2})'.format(table_name, flds, values)

    def get_statement(self, table_name):
        statement = self._statements.get(table_name)
        if not statement:
            columns = self._get_table_columns(table_name)
            statement = self._generate_statement(table_name, columns)
            self._statements[table_name] = statement
        return statement


class Database(object):
    def __init__(self):

        self.connection = get_endpoint_conn('scores')
        self.cursor = self.connection.cursor()
        self.statement_generator = StatementGenerator(self.connection)

    def __enter__(self):
        return self

    def __exit__(self, *_):
        self.connection.commit()
        if getattr(self, 'db_file', None):
            self.connection.close()

    def create_tables(self, statements):
        script = ';'.join(statements)
        cur = self.connection.cursor()
        cur.executescript(script)

    def drop_tables(self, tables):
        statements = ['DROP TABLE IF EXISTS {}'.format(t) for t in tables]
        script = ';'.join(statements)
        cur = self.connection.cursor()
        cur.executescript(script)

    def insert(self, table, values):
        statement = self.statement_generator.get_statement(table)
        self.cursor.executemany(statement, values)

    def select(self, statement, *args):
        self.cursor.execute(statement, args)
        for row in self.cursor:
            yield row

    def get_all_identities_from_table(self, table):
        statement = 'SELECT id FROM %s' % table
        for source_id, in self.select(statement):
            yield str(source_id)

    def vacuum(self):
        self.connection.execute('VACUUM')


def parse_results_file(results_path):
    schema = {
        'type': 'object',
        'properties': {
            'bets': {
                'type': 'array',
                'items': {
                    'type': 'object',
                    'properties': {
                        'bet_id': {'type': 'number'},
                        'value': {
                            'type': 'string',
                            'enum': ['1', 'X', '2']
                        },
                    }
                }
            }
        }
    }

    try:
        with open(results_path) as f:
            results = json.load(f)

        validate(results, schema)

        return results

    except (ValidationError, ValueError) as e:
        logger.exception('result json error - {}'.format(e.args))
        raise LocalValidationError('error while reading parsing input file ({})'.format(e.args))


def process_adapter(adapter, doc, source_id, db, bets_results_by_id):
    segments = adapter.get_segments(doc, bets_results_by_id)
    valid_segments = (segment for segment in segments if adapter.segment_is_valid(segment))

    pendings = []
    for segment in valid_segments:
        if isinstance(segment, dict):
            if 'source_id' not in segment:
                segment['source_id'] = source_id

        segment = adapter.get_values(segment)
        pendings.append(tuple(segment))

    if pendings:
        db.insert(table=adapter.TABLE_NAME, values=pendings)


def process_source_adapters(source, doc, source_id, db, bets_results_by_id):
    for adapter in adapters.get_source_adapters(source.SOURCE_NAME):
        if adapter.is_valid(doc):
            process_adapter(adapter, doc, source_id, db, bets_results_by_id)


def process_source(source, db, bets_results_by_id):
    mongo_db = mongo.get_connection()
    collection = getattr(mongo_db, source.MONGO_COL)

    total_docs = 0

    cursor = collection.find({'deleted': {'$ne': True}})
    for doc in cursor:
        source_id = source.get_source_id(doc)
        if source.is_valid(doc):
            process_source_adapters(source, doc, source_id, db, bets_results_by_id)

        total_docs += 1

    logger.info('processed %d docs', total_docs)


def process_all_sources(db, bets_results_by_id):
    all_sources = sources.get_all_sources()

    logger.info('starting to process all sources')

    for source_type in all_sources:
        logger.info('processing source type: %s', source_type)
        source = sources.get_source(source_type)

        process_source(source, db, bets_results_by_id)


def load_data(bets_results_by_id):
    with Database() as db:
        # todo: remove after
        tables = adapters.iter_adapter_attributes('TABLE_NAME')
        db.drop_tables(tables)

        create_statements = adapters.iter_adapter_attributes('CREATE_TABLE')
        db.create_tables(create_statements)

        process_all_sources(db, bets_results_by_id)

        db.vacuum()


def prepare_bets_values_by_id(bets_results):
    bets = bets_results['bets']
    chunk_size = 50

    result_ratio_mapper = {
        '1': 'opt_1_ratio',
        'X': 'opt_x_ratio',
        '2': 'opt_2_ratio',
    }

    bets_values_by_id = {}
    while bets:
        chunk_bets = bets[:chunk_size]
        bets = bets[chunk_size:]

        chunk_bets_ids = [b['bet_id'] for b in chunk_bets]
        chunk_bets_def = search.do_search('bets', {'_id': {'$in': chunk_bets_ids}})
        chunk_bets_def = {b['id']: b for b in chunk_bets_def}

        for cb in chunk_bets:
            res_bet_id, res_bet_value = cb['bet_id'], cb['value']
            bet_def = chunk_bets_def.get(res_bet_id)
            if not bet_def:
                raise Exception('unknown bet in results bets')

            res_ration = bet_def[result_ratio_mapper[res_bet_value]]
            bets_values_by_id[res_bet_id] = (res_bet_value, res_ration)

    return bets_values_by_id


def main(results_path):
    results_path = results_path or '/tmp/result_example1.json'

    try:
        bets_results = parse_results_file(results_path)
        bets_results_by_id = prepare_bets_values_by_id(bets_results)

        load_data(bets_results_by_id)
    except LocalValidationError as e:
        raise


if __name__ == '__main__':
    local_parser = argparse.ArgumentParser()
    local_parser.add_argument('-p', '--path', action='store', dest='path')
    args = local_parser.parse_args()

    sys.exit(main(args.path))
