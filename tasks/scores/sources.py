_sources = {}


def register(cls):
    _sources[cls.SOURCE_NAME] = cls
    return cls


def get_source(source_type):
    source = _sources.get(source_type)
    if not source:
        raise ValueError('wrong source type: %s' % source_type)
    return source()


def get_all_sources():
    return _sources.keys()


class Source(object):
    SOURCE_NAME = None

    def get_source_id(self, doc):
        return doc.get('_id')

    def is_valid(self, doc):
        return True


@register
class ClassApp(Source):
    SOURCE_NAME = 'class_app'
    MONGO_COL = 'class_app'


@register
class BetApp(Source):
    SOURCE_NAME = 'bet_app'
    MONGO_COL = 'bet_app'


@register
class Group(Source):
    SOURCE_NAME = 'group'
    MONGO_COL = 'group'
