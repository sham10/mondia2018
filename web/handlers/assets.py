import infra.handler

from web.assets import get, post


class AssetsHandler(infra.handler.Handler):
    def get(self, asset_name):
        self.finish(get(asset_name))

    def post(self, asset_name):
        data = self.load_json()
        post(asset_name, data.get('value'))
