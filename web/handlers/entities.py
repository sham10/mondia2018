import logging
import datetime

from infra6.infra5.infra4 import models

import infra.handler

from web import entities

from web.entities import users
from web.entities.entity_types.class_app import get_user_class_apps
from web.services import search
from web.reports import execute_report

_logger = logging.getLogger(__name__)


def to_dict(func):
    def inner(*args, **kwargs):
        return {k: v for k, v in func(*args, **kwargs) if v is not None}
    return inner


def _decode(value, value_type):
    if isinstance(value, list):
        return [_decode(v, value_type) for v in value]

    value = value.decode() if isinstance(value, bytes) else value
    value = value_type(value) if value_type else value

    return value


class EntityHandler(infra.handler.Handler):
    entity_type = None
    schema = {}

    def parse_data(self):
        # todo: fix me
        data = {}
        for k, v in self.request.arguments.items():
            value_type = self.schema.get(k)
            v = _decode(v, value_type)

            data[k] = v

        return data

    def context(self, **_):
        return {}

    def do_get(self, entity_id=None):
        if entity_id:
            entity_id = int(entity_id)

        data = {}
        host = self.request.host
        permissions = ['admin'] if self.credentials.admin else []
        if entity_id:
            try:
                data = entities.get(self.credentials.user_id, self.entity_type, entity_id, permissions, host)
            except entities.NotFoundError:
                raise infra.handler.HTTPError(404, log_message='entity not found {} {}'.format(self.entity_type,
                                                                                               entity_id))

        context = self.context(id=entity_id, **data)
        if context:
            data['context'] = context

        data['id'] = entity_id

        return data

    def get(self, entity_id=None):
        data = self.do_get(entity_id)
        self.render_default('{}.html'.format(self.entity_type), **data)
        #self.write_json(**data)

    def do_post(self, entity_id, data):
        if entity_id:
            entity_id = int(entity_id)

        user_id = self.credentials.user_id
        host = self.request.host
        permissions = ['admin'] if self.credentials.admin else []
        try:
            if entity_id:
                result = entities.update(user_id, self.entity_type, entity_id, permissions, host, data)
            else:
                result = entities.append(user_id, self.entity_type, permissions, host, data)
                # self.write_json(**result)
        except entities.ValidationError as ex:
            err_message, = ex.args
            raise infra.handler.JsonError(err_message)

        return result['id']

    def post(self, entity_id=None):
        data = self.parse_data()
        entity_id = self.do_post(entity_id, data)
        self.redirect('/{}/{}'.format(self.entity_type, entity_id))


class GroupHandler(EntityHandler):
    entity_type = 'group'
    schema = {
        'users': int,
        'startamount': float,
    }

    @to_dict
    def context(self, id=None, **_):
        all_users = users.get_all()
        users_by_id = {u['user_id']: u for u in all_users}
        yield 'users_by_id', users_by_id

        all_classes = search.do_search('classes')
        yield 'classes', all_classes

        user_id = self.credentials.user_id
        user_class_apps = get_user_class_apps(user_id, id)
        user_class_apps_by_class = {user_class_app['class_id']: user_class_app['id']
                                    for user_class_app in user_class_apps}
        yield 'user_class_apps', user_class_apps_by_class

        user_scores = list(execute_report('scores', user_id=user_id, group_id=id, as_dict=True))
        user_scores_by_class = {row['class_id']: row['score'] for row in user_scores}
        yield 'user_scores', user_scores_by_class

        user_balance = execute_report('users_balance', user_id=user_id, group_id=id, as_dict=True)
        user_balance = user_balance and next(user_balance)
        yield 'user_balance', user_balance.get('balance') or 'NA'

        group_scores = list(execute_report('users_balance', group_id=id, as_dict=True))
        group_scores = sorted(group_scores, key=lambda row: row['balance'] or 0, reverse=True)
        yield 'group_scores', group_scores

        yield 'current_user', user_id


class BetHandler(EntityHandler):
    entity_type = 'bet'
    schema = {
        'opt1ratio': float,
        'optxratio': float,
        'opt2ratio': float,
    }


class ClassHandler(EntityHandler):
    entity_type = 'class'
    schema = {
        'bets': int
    }

    @to_dict
    def context(self, **_):
        query = {
            'deleted': {'$exists': False}
        }
        bets = models.Bet.select(**query) or []
        bets = [{'id': b._id, 'name': b.name, 'date': b.date} for b in bets]
        bets = sorted(bets, key=lambda b: b['date'])

        yield 'bets', bets


class ClassApplicationHandler(EntityHandler):
    entity_type = 'classapplication'

    schema = {
        'amount': float
    }

    def parse_data(self):
        # todo: fix me
        data = {}
        bets = []
        for k, v in self.request.arguments.items():
            value_type = self.schema.get(k)
            v = _decode(v, value_type)

            if k.startswith('bet'):
                bets.append({k: v[0]})
            else:
                data[k] = v

        data['bets'] = bets
        return data

    def get_upcoming_class(self):
        today = datetime.datetime.now()
        query = {'date': {'$gte': today}}
        upcoming_class = search.search_one('classes', query)

        return upcoming_class and upcoming_class['id']

    @to_dict
    def context(self, id=None, class_id=None, **_):
        if id:
            class_app_query = {'_id': id}
            class_app = search.search_one('class_apps', class_app_query)
            class_id = class_app and class_app['class_id']

        today = datetime.datetime.now()
        query = {'_id': class_id} if class_id else {'date': {'$gte': today}}
        group_class = search.search_one('classes', query)
        if group_class:
            bet_ids = list(map(int, group_class['bet_ids'].split(',')))
            query = {'_id': {'$in': bet_ids}}
            class_bets = search.do_search('bets', query)

            yield 'class', group_class
            yield 'class_bets', class_bets

    def get(self, group_id=None, class_id=None):
        context = self.context(class_id=int(class_id))
        data = {
            'context': context,
            'group_id': group_id,
        }

        self.render_default('{}.html'.format(self.entity_type), **data)

    def post(self, group_id, class_id=None):
        data = self.parse_data()
        data['group_id'] = int(group_id)
        data['class_id'] = int(class_id or self.get_upcoming_class())

        class_app_id = super(ClassApplicationHandler, self).do_post(None, data)
        self.redirect('/{}/{}'.format(self.entity_type, class_app_id))


class EditClassApplicationHandler(ClassApplicationHandler):
    def get(self, entity_id):
        data = self.do_get(entity_id)
        self.render_default('{}.html'.format(self.entity_type), **data)

    def post(self, entity_id):
        if not entity_id:
            raise infra.handler.HTTPError(404, log_message='entity not found {} {}'.format(self.entity_type, entity_id))

        data = self.parse_data()
        entity_id = self.do_post(entity_id, data)
        self.redirect('/{}/{}'.format(self.entity_type, entity_id))
