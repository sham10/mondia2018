import infra.handler


class IndexHandler(infra.handler.Handler):
    CREDENTIALS_NEEDED = False

    def get(self):
        self.render_default('index.html')
