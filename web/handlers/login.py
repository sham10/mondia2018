
import infra.handler

from infra.errors import Forbidden

from web.auth import login


class LoginHandler(infra.handler.Handler):
    CREDENTIALS_NEEDED = False

    def get(self):
        self.render_default('login.html')

    def post(self, *args, **kwargs):
        try:
            email, password = (self.get_body_argument(p) for p in ('email', 'password'))
            if not email or not password:
                raise login.LoginError

            credentials = login.login(email, password)
            if not credentials:
                raise login.LoginError('invalid credentials')

            token_enc = credentials.dumps()
            self.set_cookie(self.COOKIE, token_enc, expires_days=2, httponly=True)

            self.redirect('/welcome')
        except login.LoginError:
            self.clear_all_cookies()
            raise Forbidden
        except Exception as e:
            print('login error - {}'.format(e.args))
            self.clear_all_cookies()
            self.log_error()
            raise Forbidden
