import infra.handler

from infra6.infra5.infra4.exceptions import (
    MissingRequiredValueError,
    ValidationError,
)

from web.auth.user import (
    register,
    assign_group_for_user
)


class RegisterHandler(infra.handler.Handler):
    CREDENTIALS_NEEDED = False
    arguments = {
        'first_name': 'firstname',
        'last_name': 'lastname',
        'email': 'email',
        'password': 'password',
        're_password': 'retypepassword',
    }

    def get(self, token=None):
        if token and self.credentials:
            assign_group_for_user(self.credentials.user_id, token)
            self.redirect('/welcome')

        self.render_default('register.html', error='', token=token)

    def post(self, token=None):
        try:
            data = {k: self.get_argument(v) for k, v in self.arguments.items()}
            register(data, token)
            #self.redirect('/post_registration')
            self.redirect('/login')
        except(MissingRequiredValueError, ValidationError) as e:
            error_msg = e.args[0]
            self.render_default('register.html', error=error_msg)
