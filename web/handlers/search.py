import logging

import infra.handler
from web.services import search

_logger = logging.getLogger(__name__)


class SearchHandler(infra.handler.Handler):

    def get(self, entity_type):
        data = search.do_search(entity_type)
        self.render_default('{}.html'.format(entity_type), data=data)
