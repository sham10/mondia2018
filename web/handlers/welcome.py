from infra6.infra5.infra4.models import (
    UserACL,
    Group
)

import infra.handler


class WelcomeHandler(infra.handler.Handler):

    def get_user_groups(self):
        user_credentials = self.credentials
        user_access = UserACL.select(user_id=user_credentials.user_id)
        user_group_ids = [ua.group_id for ua in user_access]

        user_groups = Group.select(_id={'$in': user_group_ids})
        user_groups = [{'group_id': ug._id, 'group_name': ug.name} for ug in user_groups]

        return user_groups

    def get(self):
        user_groups = self.get_user_groups()
        self.render_default('welcome.html', user_groups=user_groups or None, current_user='{} {}'.format(
            self.credentials.first_name.decode(), self.credentials.last_name.decode()))
