import http.client
from tornado.web import HTTPError


class Forbidden(HTTPError):
    def __init__(self, message=None):
        super(Forbidden, self).__init__(http.client.FORBIDDEN, message)


class NotFound(HTTPError):
    def __init__(self, message=None):
        super(NotFound, self).__init__(http.client.NOT_FOUND, message)


class NotAllowed(HTTPError):
    def __init__(self, message=None):
        super(NotAllowed, self).__init__(http.client.METHOD_NOT_ALLOWED, message)
