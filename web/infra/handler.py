#!/usr/bin/env python3

import json
import decimal
import datetime
import logging
import http.client
import types

from functools import wraps

import tornado.web
from tornado.web import HTTPError

import redis

from infra6.infra5.infra4 import constants
from infra6.infra5.infra4 import signs
from infra6.infra5.infra4 import config

from web.auth import login
from web.auth.login import Credentials

CONTENT_TYPE_JSON = 'application/json; charset=UTF-8'
CONTENT_TYPE_TEXT = 'text/plain'
CONTENT_TYPE_JS = 'text/javascript'


class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        if isinstance(o, datetime.datetime):
            return o.strftime(constants.DATETIME_FORMAT)
        if isinstance(o, datetime.date):
            return o.strftime(constants.DATE_FORMAT)


class JsonError(Exception):
    def __init__(self, message):
        self.message = message


class Handler(tornado.web.RequestHandler):
    _WRAP_ = True

    user = None
    CREDENTIALS_NEEDED = True

    COOKIE = 's'

    def write_error(self, status_code, **kwargs):
        pass

    def log_error(self):
        req = self.request

        event_info = {
            'sentry.interfaces.Http': {
                'url': req.full_url(),
                'method': req.method,
                'body': req.body,
                'headers': dict((k, v) for k, v in req.headers.items() if k not in ['t', 't1']),
            }
        }

        if self.credentials:
            event_info['sentry.interfaces.User'] = {
                'id': self.credentials.user_id,
                'email': self.credentials.email,
                'username': '%s %s' % (self.credentials.first_name, self.credentials.last_name),
                'is_authenticated': True
            }

        print(event_info)

    def _request_summary(self):
        try:
            user = self.credentials and self.credentials.email or 'anonymous [{}]'.format(self.request.remote_ip)
            user_agent = self.request.headers.get('User-Agent') or ''
            return ' '.join((user, self.request.method, self.request.uri, user_agent))
        except:
            return super(Handler, self)._request_summary()

    def is_rate_exceeded(self):
        user_id = self.credentials and self.credentials.user_id or 0

        if user_id:
            if self.credentials.email == 'badihio@gmail.com':
                return

            if self.get_argument('socialenl', None):
                return

        key = 'LIMIT:{user}'.format(user=user_id)

        try:
            client = redis.StrictRedis(config.REDIS_HOST, db=1)

            script = '''
              local key = KEYS[1]
              local value = redis.call('INCR', key)

              if value == 1 then
                local ttl = KEYS[2]
                redis.call('EXPIRE', key, ttl)
              end

              return value
            '''

            value = client.eval(script, 2, key, 10)
            if value > 40:
                return True
        except:
            pass

    def init_debug_logger(self, debug_handlers):
        logger = logging.getLogger(None)
        for debug_handler in debug_handlers:
            logger.addHandler(debug_handler)
        logger.setLevel(logging.DEBUG)

    def stop_debug_logger(self, debug_handlers):
        logger = logging.getLogger(None)
        for debug_handler in debug_handlers or []:
            logger.removeHandler(debug_handler)
        logger.setLevel(logging.INFO)

    def http_method(self, func):

        def wrap(*args, **kwargs):
            if self.credentials:
                self.set_header('x-user', self.credentials.user_id)
            try:
                func(*args, **kwargs)
            except tornado.web.HTTPError:
                raise
            except JsonError as ex:
                self.write_json(success=False, message=ex.message)
            except:
                self.log_error()
                raise
            finally:
                pass

        return wrap

    def set_default_headers(self):
        self.set_header('Server', 'sociale')

    def parse_token_from_cookie(self, token):
        token_string = token and signs.decrypt(token.encode(), sep=None)
        return token_string and Credentials.loads(token_string)

    def prepare(self):
        methods = [
            (lambda: self.get_cookie(self.COOKIE), self.parse_token_from_cookie),
            (lambda: self.get_argument('auth', None), login.api_login)
        ]

        self.credentials = None

        try:
            for auth_getter, method in methods:
                auth_value = auth_getter()
                if auth_value:
                    self.credentials = method(auth_value)
                    break
        except login.LoginError:
            raise tornado.web.HTTPError(403)

        if self.CREDENTIALS_NEEDED:
            if not self.credentials:
                raise tornado.web.HTTPError(403)

        if self._WRAP_:
            self.get = self.http_method(self.get)
            self.post = self.http_method(self.post)
            self.delete = self.http_method(self.delete)

    def write_raw(self, content_type, value):
        self.set_header("Content-Type", content_type)
        self.finish(value)

    def write_text(self, value):
        self.set_header("Content-Type", CONTENT_TYPE_TEXT)
        self.finish(value)

    def write_json(self, *args, **kwargs):
        if args and kwargs:
            raise Exception('conflict')

        self.set_header("Content-Type", CONTENT_TYPE_JSON)
        response = json.dumps(args or kwargs, cls=CustomEncoder)
        self.finish(response)

    def render_default(self, template_name, **kwargs):
        self.render(template_name, **kwargs)

    def load_json(self):
        body = self.request.body
        try:
            return body and json.loads(body.decode('utf-8'))
        except ValueError:
            raise JsonError('invalid json')


class NotFoundHandler(Handler):
    CREDENTIALS_NEEDED = False

    def get(self):
        self.set_status(404)
        self.write_text(self.request.uri)

    def post(self, *args, **kwargs):
        self.set_status(404)
        self.write_text(self.request.uri)


def http_response(f):
    @wraps(f)
    def wrapper(self, *args):
        try:
            result = f(self, *args)
            if isinstance(result, types.GeneratorType):
                result = dict(next(result))
        except HTTPError:
            raise
        except Exception as ex:
            self.set_status(http.client.BAD_REQUEST)
            result = {'success': False, 'message': ex.message}

        result = result or {}
        self.write_json(**result)

    return wrapper
