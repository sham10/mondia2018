#!/usr/bin/env python3

import signal
import sys
import os
import logging

from zmq.eventloop import ioloop

from tornado import options
from tornado.options import define

import tornado.httpserver
import tornado.web
import tornado.ioloop
import tornado.netutil

from infra import handler
from handlers import (
    index,
    entities,
    assets,
    login,
    register,
    welcome,
    search
)

here = os.path.split(__file__)[0]

ioloop.install()

define('port', os.environ.get('PORT', 8080), int)
define('debug', False, bool)
define('socket', None, str)
options.parse_command_line()


def get_handlers():
    yield '/', index.IndexHandler

    yield '/login', login.LoginHandler
    yield '/register', register.RegisterHandler
    yield '/register/(\w+)', register.RegisterHandler

    yield '/welcome', welcome.WelcomeHandler

    yield '/group', entities.GroupHandler
    yield '/group/(\d+)', entities.GroupHandler

    yield '/bet', entities.BetHandler
    yield '/bet/(\d+)', entities.BetHandler

    yield '/class', entities.ClassHandler
    yield '/class/(\d+)', entities.ClassHandler

    yield '/search/(bets|classes|groups)', search.SearchHandler

    yield '/class/apply/(\d+)', entities.ClassApplicationHandler
    yield '/class/apply/(\d+)/(\d+)', entities.ClassApplicationHandler
    yield '/classapplication/(\d+)', entities.EditClassApplicationHandler

    yield '.*', handler.NotFoundHandler


def main():
    for l in ('requests.packages.urllib3.connectionpool',
              'requests', 'urllib3.connectionpool'):
        requests_log = logging.getLogger(l)
        requests_log.setLevel(logging.WARNING)

    def stop(*_):
        tornado.ioloop.IOLoop.instance().stop()

    signal.signal(signal.SIGINT, stop)

    opt = options.options
    application = tornado.web.Application(get_handlers(),
                                          debug=opt.debug,
                                          cookie_secret='yyy',
                                          template_path=os.path.join(here, 'templates'))

    server = tornado.httpserver.HTTPServer(application, xheaders=True)
    if opt.socket:
        socket = tornado.netutil.bind_unix_socket(opt.socket, mode=0o666)
        server.add_socket(socket)
    else:
        server.listen(options.options.port)

    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    sys.exit(main())
