from infra6.infra5.infra4.config import (
    get_config,
    set_config
)


def get(asset_name):
    config_key = 'asset_{}'.format(asset_name)
    asset_value = get_config(config_key)

    return asset_value


def post(asset_name, value):
    config_key = 'asset_{}'.format(asset_name)
    set_config(config_key, value)
