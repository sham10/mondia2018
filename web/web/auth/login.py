
import time
import msgpack
import datetime
import logging

from infra6.infra5.infra4.models import User

from infra6.infra5.infra4 import (
    utils,
    signs,
    mongo
)

_logger = logging.getLogger(__name__)


class LoginError(Exception):
    pass


class Credentials(object):

    def __str__(self):
        return '<< {} >>'.format(self.email)

    def __init__(self, user_id, email, first_name, last_name, admin, acl=None):
        self.user_id = user_id
        self.email = email
        self.first_name = first_name
        self.last_name = last_name
        self.admin = admin
        self.acl = acl or {}

    @classmethod
    def loads(cls, token_string):
        value = msgpack.loads(token_string)

        mappings = (
            (b'i', 'user_id'),
            (b'e', 'email'),
            (b'f', 'first_name'),
            (b'l', 'last_name'),
            (b'd', 'admin'),
        )

        credentials_values = {key: value.get(src) for src, key in mappings}
        acl = value.get(b'a')

        return Credentials(acl=acl, **credentials_values)

    def dumps(self):
        token = {
            b't': time.time(),
            b'i': self.user_id,
            b'e': self.email,
            b'f': self.first_name,
            b'l': self.last_name,
            b'd': self.admin,
            b'a': msgpack.dumps(self.acl),
        }

        token_string = msgpack.dumps(token)
        return signs.encrypt(token_string)


def get_password_hash(password):
    return utils.get_hash('p_a_s', password)


def allowed(email, user=None):
    try:
        if not user:
            user = User.get({'email': email})
            if not user:
                raise LoginError

        return True
    except LoginError:
        pass


def _get_credentials(user):
    user_id = user._id
    admin = user.admin or False

    mongo_conn = mongo.get_connection()
    user_acl = list(mongo_conn.user_acl.find({'user_id': user_id}))
    user_acl = [{a['group_id']: a.get('admin', False)} for a in user_acl]

    return Credentials(user_id, user.email, user.first_name, user.last_name, admin, user_acl)


def _update_last_login(user):
    last_signin = user.last_signin
    today = datetime.date.today()
    today_dt = datetime.datetime(today.year, today.month, today.day)
    if not last_signin or last_signin < today_dt:
        user.last_signin = today_dt
        user.put()


def api_login(token):
    user = User.get({'api_key': token})
    if not user:
        raise LoginError('no user')

    credentials = _get_credentials(user)
    _update_last_login(user)

    return credentials


def login(email, password):
    user = User.get({'email': email})
    if not user:
        raise LoginError('no user')

    if not user.active:
        raise LoginError('user is not active')

    if user.password != get_password_hash(password):
        raise LoginError('wrong password')

    credentials = _get_credentials(user)
    _update_last_login(user)
    return credentials


def get_forgot_token(email):
    token = signs.encrypt(time.time(), email)
    return token


def reset_password(email, password, token):
    decrypted = signs.decrypt(token)
    if not decrypted:
        return

    _, token_email = decrypted
    if token_email != email:
        return

    user = User.get({'email': email})
    if not allowed(None, user):
        return

    user.password = get_password_hash(password)
    user.put()
    return True
