from infra6.infra5.infra4.exceptions import (
    MissingRequiredValueError,
    ValidationError,
)
from infra6.infra5.infra4.models import User
from infra6.infra5.infra4 import mongo
from infra6.infra5.infra4.signs import decrypt
from infra6.infra5.infra4.data.data import allocate_id
from .login import get_password_hash


def user_exists(email):
    return User.find(email=email)


def validate(data):
    for k, v in data.items():
        if not v:
            raise MissingRequiredValueError(k)

    if user_exists(data['email']):
        raise ValidationError('user already exists, please choose different one')

    if data['password'] != data['re_password']:
        raise ValidationError('password and re type password do not match')


def assign_group_for_user(user_id, token):
    group_id, registration_date = decrypt(token)
    group_id = group_id and int(group_id)

    mongo_conn = mongo.get_connection()
    user_acl = mongo_conn.user_acl

    user_acl.insert(
        [{
            'group_id': group_id,
            'user_id': user_id
        }]
    )

    group_coll = mongo_conn.group
    group = group_coll.find_one({'_id': group_id})
    group_members = group.get('users', [])
    group_members.append(user_id)
    group_coll.update_one({'_id': group_id}, {'$set': {'users': list(set(group_members))}}, upsert=True)


def register(data, token=None):
    validate(data)

    model_data = data.copy()
    model_data['password'] = get_password_hash(model_data['password'])
    model_data['admin'] = False
    model_data['active'] = True

    identity = allocate_id()
    try:
        model = User(identity, load=False, **model_data)
    except ValueError as e:
        raise ValidationError('invalid data type: {}'.format(e.args[0]))

    model.put()

    if token:
        assign_group_for_user(identity, token)
