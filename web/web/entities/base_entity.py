
import logging
from collections import defaultdict

from .property import Property
from .helpers import to_dict
from .validation_exceptions import ValidationError
from .helpers import IGNORE
import collections

_logger = logging.getLogger('base_entity')


class ReadOnlyProperty(Property):
    def get_model_value(self, value, _):
        raise ValidationError("Can't set value for read only property {}".format(self._entity_name))


class EntityClass(type):
    def __new__(mcs, name, bases, dct):
        return type.__new__(mcs, name, bases, dct)

    def __init__(cls, name, bases, dct):
        cls._by_entity = by_entity = {}
        cls._by_model = by_model = defaultdict(list)

        for base in bases:
            if hasattr(base, '_by_entity'):
                by_entity.update(base._by_entity)

            if hasattr(base, '_by_model'):
                by_model.update(base._by_model)

        for key, prop in dct.items():
            if isinstance(prop, Property):
                prop.configure(key)
                by_model[prop._model_name].append(prop)
                if prop._read_only:
                    by_entity[prop._entity_name] = ReadOnlyProperty(entity_name=prop._entity_name)
                else:
                    by_entity[prop._entity_name] = prop

        super(EntityClass, cls).__init__(name, bases, dct)


class ComplexProperty(Property, metaclass=EntityClass):
    _by_entity = None
    _by_model = None
    components = ()

    def _validate_container_type(self, prop, val):
        if val is not None and isinstance(val, list) is not (prop._entity_container_type is list):
            err_str = '{} type should be of type {}'.format(prop._entity_name, prop._entity_container_type)
            _logger.warning(err_str)
            raise ValidationError(err_str)

    def get_model_value(self, data, context):
        result = {}
        deferred = []

        if data is None:
            return None

        if not isinstance(data, dict):
            raise ValidationError('Entity data should be a dictionary')

        prop_data = ((self._by_entity[k], v) for k, v in data.items() if k in self._by_entity)
        try:
            for prop, val in prop_data:

                # todo: remove after list values fix
                if val is not None and isinstance(val, list) is not (prop._entity_container_type is list):
                    val = val[0]

                self._validate_container_type(prop, val)

                if isinstance(val, list):
                    if not prop.allow_duplicates:
                        val = list(set(val))

                    model_val = [prop.get_model_value(v, context) for v in val]
                    has_items = bool(model_val)
                    model_val = [v for v in model_val if v is not IGNORE]

                    if has_items and not model_val:
                        model_val = IGNORE
                else:
                    model_val = prop.get_model_value(val, context)

                if isinstance(model_val, collections.Callable):
                    deferred.append((prop._model_name, model_val))
                elif model_val is not IGNORE:
                    result[prop._model_name] = model_val

            result.update([v for c in self.components for v in c.get_model_value(context=context, **data) or []])

            for model_name, func in deferred:
                a_val = func(context)
                result[model_name] = a_val

        except ValidationError as e:
            msg = ':'.join([self._entity_name, e.args[0]]) if self._entity_name else e.args[0]
            raise ValidationError(msg)

        return result

    _empty = (None, [])

    def prepare_entity_data(self, data):
        return ((p, v) for k, v in data.items() if k in self._by_model and v not in self._empty and
                not k.startswith('_')
                for p in self._by_model[k])

    @to_dict
    def get_entity_value(self, data, context):
        prop_data = self.prepare_entity_data(data)

        for prop, val in prop_data:
            if not isinstance(val, list) or prop._entity_container_type is not list:
                entity_val = prop.get_entity_value(val, context)
            else:
                entity_val = [prop.get_entity_value(v, context) for v in val]

            yield prop._entity_name, entity_val

        for comp in self.components:
            for name, val in comp.get_entity_value(context=context, **data):
                yield name, val
