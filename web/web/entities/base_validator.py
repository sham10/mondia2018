
import inspect
import logging

from .validation_exceptions import MissingRequiredValueError

logger = logging.getLogger(__name__)


def get_method_args(func):
    arg_spec = inspect.getargspec(func)
    return arg_spec.args[1:], arg_spec.keywords


def get_methods(instance, method_type):
    for func_name, func in inspect.getmembers(instance, inspect.ismethod):
        if func_name.startswith(method_type):
            yield func


class BaseValidator(object):
    _entity_type = None
    _unique_fields = []
    _required_fields_mapping = {}
    _required_roles_mapping = {}

    def __init__(self, context):
        self._context = context

    def initialize(self, data):
        pass

    def required_fields_validation(self, data, required_fields_mapping):
        for section, required_fields in required_fields_mapping.items():
            if not section:
                for field in required_fields:
                    model_value = data.get(field)

                    if model_value in (None, [], {}, ''):
                        raise MissingRequiredValueError(field)
            else:
                section_data = data.get(section)
                if section_data:
                    section_required_fields = {None: required_fields}
                    for item in section_data:
                        self.required_fields_validation(item, section_required_fields)

    def _enforce_customized_validations(self, data, context):
        return True

    def validate(self, data, context):
        self.initialize(data)

        self.required_fields_validation(data, self._required_fields_mapping)

        if not self._enforce_customized_validations(data, context):
            return

        for method in get_methods(self, 'validate_'):
            method_args, _ = get_method_args(method)

            model_data = [data.get(arg) for arg in method_args]
            if model_data and not all(i is None for i in model_data):
                method(*model_data)
