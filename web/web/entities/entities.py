import datetime
import logging

from . import hooks

from infra6.infra5.infra4.data.data import allocate_id
from .helpers import merge
from .validation_exceptions import ValidationError, NotFoundError
from infra6.infra5.infra4 import models
from web.entities.entity_types import (
    Bet, BetValidator,
    Group, GroupValidator,
    Class, ClassValidator,
    ClassApp, ClassAppValidator
)


log = logging.getLogger(__name__)


mappings = {
    'bet': (models.Bet, Bet, BetValidator),
    'group': (models.Group, Group, GroupValidator),
    'class': (models.Class, Class, ClassValidator),
    'classapplication': (models.ClassApplication, ClassApp, ClassAppValidator),
}


class Context(object):
    def __init__(self, user_id, identity, user_permissions=None, host=None, prev_model=None):
        self.user_id = user_id
        self.identity = identity
        self.prev_model = prev_model
        self.user_permissions = user_permissions or []
        self.now = None
        self.response = {}
        self.host = host


def append(user_id, entity_type, user_permissions, host, data):
    print('new entity data - {}'.format(data))
    model_cls, mapper_cls, validator_type = mappings.get(entity_type)

    context = Context(user_id, None, user_permissions, host)
    mapper = mapper_cls()
    model_data = mapper.get_model_value(data, context)
    log.debug('{} model data - {}'.format(entity_type, model_data))
    validator = validator_type(context)
    validator.validate(model_data, context)

    context.identity = identity = allocate_id()
    context.now = datetime.datetime.now()
    hooks.execute_hooks(entity_type, model_data, context)
    try:
        print('model data - {}'.format(model_data))
        model = model_cls(identity, load=False, **model_data)
    except ValueError as e:
        raise ValidationError('invalid data type: {}'.format(e.args[0]))

    model.put()
    print('added new entity ({},{})'.format(entity_type, identity))
    #TODO: consider tasks
    #queue.Client('adk2tasks').entities3.save(network, user_id, entity_type, identity, data)

    response = context.response
    response.update({'id': identity})
    return response


def _merge_model_data(model, model_update_data, context):
    model_current_data = model.save()
    try:
        return merge(model_update_data, model_current_data)
    except Exception:
        raise ValidationError('The received data is not in the correct format')


def update(user_id, entity_type, identity, user_permissions, host, data):
    print(data)
    model_cls, mapper_cls, validator_type = mappings.get(entity_type)

    mapper = mapper_cls()
    model = model_cls.get(identity)
    if not model:
        raise ValidationError('entity id {} not found'.format(identity))

    context = Context(user_id, identity, user_permissions, host, prev_model=model)

    model_update_data = mapper.get_model_value(data, context)

    response = context.response
    response.update({'id': identity})
    model_data = _merge_model_data(model, model_update_data, context)

    if not model_data:
        return response

    if model_data.get('deleted'):
        raise ValidationError('Entity deleted - editing is not allowed')

    validator = validator_type(context)
    validator.validate(model_data, context)

    context.now = datetime.datetime.now()
    hooks.execute_hooks(entity_type, model_data, context)
    try:
        model.update(**model_data)
    except ValueError as e:
        raise ValidationError('invalid data type: {}'.format(e.args[0]))

    model.put()

    #TODO: consider tasks
    #queue.Client('adk2tasks').entities3.save(network, user_id, entity_type, identity, data)

    return response


def do_get(entity_type, context, entity_id):
    model_cls, entity_cls, _ = mappings.get(entity_type)

    entity = entity_cls()

    model = model_cls.get(entity_id)

    model_data = model.save()

    value = entity.get_entity_value(model_data, context)

    return value


def get(user_id, entity_type, entity_id, user_permissions, host):
    context = Context(user_id, entity_id, user_permissions, host)
    entity_data = do_get(entity_type, context, entity_id)
    return entity_data
