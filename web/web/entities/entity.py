from .base_entity import ComplexProperty
from .primitives import ReadOnly, StringProperty
from .properties import UserProperty


class CustomProperties(ComplexProperty):
    key = StringProperty()
    value = StringProperty()


class Entity(ComplexProperty):
    created = ReadOnly()
    modified = ReadOnly()
    created_by = UserProperty(entity_name='createdBy', read_only=True)
    modified_by = UserProperty(entity_name='modifiedBy', read_only=True)

    flags = StringProperty(entity_container_type=list)
    deleted = ReadOnly()
