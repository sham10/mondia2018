from web.entities.entity import Entity
from web.entities.primitives import StringProperty, FloatNumericBound, DateTimeProperty


class Bet(Entity):
    name = StringProperty()
    type = StringProperty()

    date = DateTimeProperty()

    opt_1_name = StringProperty(entity_name='opt1name')
    opt_1_ratio = FloatNumericBound(entity_name='opt1ratio', min_bound=0, max_bound=1000)

    opt_x_name = StringProperty(entity_name='optxname')
    opt_x_ratio = FloatNumericBound(entity_name='optxratio', min_bound=0, max_bound=1000)

    opt_2_name = StringProperty(entity_name='opt2name')
    opt_2_ratio = FloatNumericBound(entity_name='opt2ratio', min_bound=0, max_bound=1000)
