import re
import datetime

from web.entities.entity import Entity
from web.entities.property import Property
from web.entities.primitives import (
    NonNegativeInt,
    FloatNumericBound
)
from web.entities.validation_exceptions import ValidationError
from web.services.search import search_one

from web.services import search


class BetApplication(Property):
    bet_pattern = re.compile('^bet_(.*)')

    def get_entity_value(self, bet_app, context):
        _keys = ('bet_id', 'value', 'submit_date')
        out_bet = {k: bet_app.get(k) for k in _keys}

        bet = search_one('bets', {'_id': bet_app.get('bet_id')})
        _bet_keys = ('name', 'opt_1_name', 'opt_1_ratio', 'opt_x_name',
                     'opt_x_ratio', 'opt_2_name', 'opt_2_ratio')
        bet = {k: bet.get(k) for k in _bet_keys}

        out_bet.update(bet)
        return out_bet

    def get_model_value(self, values, context):
        values = values or {}

        for bet_name, bet_value in values.items():
            match = self.bet_pattern.match(bet_name)
            if match:
                bet_id = int(match.groups()[0])
            else:
                raise ValidationError('bet value not in the right pattern ("{}")'.format(bet_name))

            return {
                'bet_id': bet_id,
                'value': bet_value,
            }


class ClassApp(Entity):
    group_id = NonNegativeInt()
    class_id = NonNegativeInt()

    amount = FloatNumericBound(min_bound=0.1, min_inclusive=True)

    bets = BetApplication(entity_name='bets', entity_container_type=list)


def get_user_class_app(user_id, group_id, class_id=None):
    today = datetime.datetime.now()
    _class = search.search_one('classes', {'class_id': class_id} if class_id else {'date': {'$gte': today}})
    class_id = _class and _class['id']

    query = {
        'user_id': user_id,
        'group_id': group_id,
        'class_id': class_id,
    }
    return search.search_one('class_apps', query)


def get_user_class_apps(user_id, group_id):
    query = {
        'user_id': user_id,
        'group_id': group_id,
    }
    return search.do_search('class_apps', query)
