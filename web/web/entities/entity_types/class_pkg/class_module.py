from web.entities.entity import Entity
from web.entities.primitives import StringProperty, DateTimeProperty, NonNegativeInt


class Class(Entity):
    name = StringProperty()

    date = DateTimeProperty()

    bet_ids = NonNegativeInt(entity_name='bets', entity_container_type=list)
