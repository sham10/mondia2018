import datetime

from infra6.infra5.infra4 import signs
from infra6.infra5.infra4.constants import DATETIME_FORMAT

from web.entities.entity import Entity
from web.entities.primitives import (
    StringProperty,
    NonNegativeInt,
    FloatNumericBound
)


class RegistrationLink(object):
    _url_format = 'http://{host}/register/{token}'

    @classmethod
    def get_model_value(cls, **_):
        return

    @classmethod
    def get_entity_value(cls, context, **_):
        group_id = context.identity
        now_str = datetime.datetime.now().strftime(DATETIME_FORMAT)
        token = signs.encrypt(group_id, now_str).decode()
        yield 'registrationLink', cls._url_format.format(host=context.host, token=token)


class Group(Entity):
    components = [RegistrationLink]

    name = StringProperty()
    users = NonNegativeInt(entity_container_type=list)
    start_amount = FloatNumericBound(entity_name='startamount')
