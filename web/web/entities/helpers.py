
from collections import OrderedDict

IGNORE = object()

EMPTY = object()


def to_list(func):
    def inner(*args):
        return [v for v in func(*args) if v is not None]
    return inner


def to_dict(func):
    def inner(*args):
        return {k: v for k, v in func(*args) if v is not None}
    return inner


def merge(source, dest):
    if source is EMPTY:
        return dest

    if source is None:
        return source

    if isinstance(source, dict):
        result = source.copy()
        for key, value in dest.items():
            source_value = source.get(key, EMPTY)

            if isinstance(value, (dict, list)):
                value = merge(source_value, value)
            else:
                if source_value is not EMPTY and source_value != value:
                    value = source_value

            result[key] = value

        return result

    if isinstance(source, list):
        if dest and all(isinstance(item, dict) and 'id' in item for item in dest):
            source_to_add = []
            source_to_merge = {}
            source_to_delete = []
            for item in source:
                identity = item.get('id')
                if identity:
                    if len(item) > 1:
                        source_to_merge[identity] = item
                    else:
                        source_to_delete.append(identity)
                else:
                    source_to_add.append(item)

            dest_dict = OrderedDict({item['id']: item for item in dest if item['id'] not in source_to_delete})
            dest_dict = merge(source_to_merge, dest_dict)
            dest = list(dest_dict.values())
            dest.extend(source_to_add)

            return dest

        return source
