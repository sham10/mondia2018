from infra6.infra5.infra4 import mongo


def group_hooks(model_data, context):
    current_user = context.user_id
    group_id = context.identity
    group_users = model_data['users']

    group_keys = ('user_id', 'group_id', 'admin')
    docs = [
        {k: v for k, v in zip(group_keys, (current_user, group_id, True))}
    ]

    for group_user in group_users:
        docs.append({k: v for k, v in zip(group_keys, (group_user, group_id, False))})

    mongo_conn = mongo.get_connection()
    user_acl = mongo_conn.user_acl

    user_acl.remove({'group_id': group_id})

    user_acl.insert(docs)


def class_app_hooks(model_data, context):
    model_data['user_id'] = context.user_id


_hooks_factory = {
    'group': group_hooks,
    'classapplication': class_app_hooks,
}


def execute_hooks(entity_type, model_data, context):
    entity_hooks_fn = _hooks_factory.get(entity_type)
    if entity_hooks_fn:
        entity_hooks_fn(model_data, context)
