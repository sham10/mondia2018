
import datetime
import json

from infra6.infra5.infra4 import constants

from .property import Property
from .validation_exceptions import ValidationError, InvalidDataTypeError


strip = str.strip
lower = str.lower
upper = str.upper
title = str.title


class ReadOnly(Property):
    def __init__(self, entity_name=None, model_name=None):
        super(ReadOnly, self).__init__(entity_name=entity_name, model_name=model_name, read_only=True)


class TypedProperty(Property):
    _entity_data_type = None
    _to_model_formatters = []
    _empty_values = (None, '')

    def __init__(self, to_model_formatters=None, **kwargs):
        super(TypedProperty, self).__init__(**kwargs)
        if to_model_formatters:
            self._to_model_formatters = to_model_formatters

    def assert_data_type(self, value):
        if self._entity_data_type and not isinstance(value, self._entity_data_type):
            raise InvalidDataTypeError(self._entity_name, value)
        return value

    def configure(self, name):
        super(TypedProperty, self).configure(name)
        self.register_prepare_action(self.assert_data_type)
        self.register_prepare_action(*self._to_model_formatters)


class StringProperty(TypedProperty):
    _entity_data_type = str
    _to_model_formatters = [strip]


class StringPropertyAllowEmpty(StringProperty):
    _empty_values = (None,)


class DateTimeProperty(StringProperty):
    PATTERN = ['%Y-%m-%d %H:%M', constants.DATETIME_FORMAT, constants.DATE_FORMAT]

    def convert(self, value):
        for pattern in self.PATTERN:
            try:
                return datetime.datetime.strptime(value, pattern)
            except ValueError:
                pass

        raise ValidationError('invalid value "{0}"'.format(value))

    def configure(self, name):
        super(DateTimeProperty, self).configure(name)
        self.register_prepare_action(self.convert)


class DateProperty(DateTimeProperty):
    PATTERN = ['%Y-%m-%d %H:%M', constants.DATE_FORMAT, constants.DATETIME_FORMAT]

    def configure(self, name):
        super(DateProperty, self).configure(name)
        self.register_prepare_action(lambda v: datetime.datetime.combine(v.date(), datetime.time(0)))

    def _get_entity_value(self, value, _):
        if isinstance(value, datetime.datetime):
            return value.date()
        return value


class BoolProperty(TypedProperty):
    _entity_data_type = bool


class NonNegativeNumber(TypedProperty):
    _entity_data_type = (float, int, int)

    def validate_not_negative(self, value):
        if value < 0:
            raise ValidationError('''"{}" can't be negative'''.format(value))
        return value

    def configure(self, name):
        super(NonNegativeNumber, self).configure(name)
        self.register_prepare_action(self.validate_not_negative)


class NonNegativeInt(NonNegativeNumber):
    def validate_int(self, value):
        int_value = int(value)
        if int_value != value:
            raise ValidationError('''"{}" should be an integer'''.format(value))
        return int_value

    def configure(self, name):
        super(NonNegativeInt, self).configure(name)
        self.register_prepare_action(self.validate_int)


class Identity(Property):
    def validate(self, value):
        try:
            return int(value)
        except ValueError:
            raise ValidationError('''"{}" should be an integer'''.format(value))

    def configure(self, name):
        super(Identity, self).configure(name)
        self.register_prepare_action(self.validate)


class ChoiceProperty(StringProperty):

    def __init__(self, choices=None, **kwargs):
        super(ChoiceProperty, self).__init__(**kwargs)
        if choices:
            self.choices = choices

    def get_valid_values(self, context):
        return self.choices

    def validate(self, value, context):
        valid_values = self.get_valid_values(context) or []
        if isinstance(value, str):
            if value not in valid_values:
                msg = '"{0}" is not a valid value'.format(value)
                raise ValidationError(msg)
        else:
            invalid_values = set(value) - set(valid_values)
            if invalid_values:
                msg = '"{0}" are not valid values'.format(', '.join(invalid_values))
                raise ValidationError(msg)

        return value

    def configure(self, name):
        super(ChoiceProperty, self).configure(name)
        self.register_prepare_action(self.validate)


class FloatNumericBound(NonNegativeNumber):

    def __init__(self, min_bound=0, max_bound=float('inf'), min_inclusive=True, max_inclusive=True, **kwargs):
        super(FloatNumericBound, self).__init__(**kwargs)
        self._min_bound = min_bound
        self._max_bound = max_bound

        self._min_inclusive = min_inclusive
        self._max_inclusive = max_inclusive

    def validate_in_bounds(self, value):
        if self._min_inclusive and value < self._min_bound:
            msg = 'should be at least {0}'.format(self._min_bound)
            raise ValidationError(msg)

        if not self._min_inclusive and value <= self._min_bound:
            msg = 'should be bigger than {0}'.format(self._min_bound)
            raise ValidationError(msg)

        if self._max_inclusive and value > self._max_bound:
            msg = 'should not be bigger than {0}'.format(self._max_bound)
            raise ValidationError(msg)

        if not self._max_inclusive and value >= self._max_bound:
            msg = 'should be smaller than {0}'.format(self._max_bound)
            raise ValidationError(msg)

        return value

    def configure(self, name):
        super(FloatNumericBound, self).configure(name)
        self.register_prepare_action(self.validate_in_bounds)


class IntNumericBound(NonNegativeInt, FloatNumericBound):
    pass


class ObjectProperty(TypedProperty):
    _entity_data_type = dict

    def _get_entity_value(self, value, _):
        return {k: v for k, v in value.items() if v is not None}


class JsonProperty(Property):
    def get_entity_value(self, value, _):
        return json.loads(value)

    def get_model_value(self, value, _):
        return json.dumps(value)
