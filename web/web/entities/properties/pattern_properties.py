
import re
import urllib.parse

from web.entities.primitives import StringProperty, strip, lower
from web.entities.validation_exceptions import ValidationError


class PatternProperty(StringProperty):
    _pattern = None

    def validate(self, value):
        if not self._pattern.match(value):
            raise ValidationError('"{}" is not a valid value'.format(value))
        return value

    def configure(self, name):
        super(PatternProperty, self).configure(name)
        self.register_prepare_action(self.validate)


class EmailProperty(PatternProperty):
    _to_model_formatters = [strip, lower]
    _pattern = re.compile('[^@|^\s]+@[^@|^\s]+\.[^@|^\s]+')


class DomainProperty(PatternProperty):
    _to_model_formatters = [strip, lower]
    _pattern = re.compile('[^.]+\.[^.]+')

    def validate(self, value):
        corrected = value if value.lower().startswith('http') else ''.join(['http://', value])

        scheme, netloc, path = urllib.parse.urlparse(corrected)[:3]

        if not scheme or not netloc or path or not self._pattern.match(netloc):
            raise ValidationError('{} is not a valid value'.format(value))
        return netloc


class UrlProperty(PatternProperty):
    _to_model_formatters = [strip]
    _pattern = re.compile('[^.]+\.[^.]+')

    def validate(self, value):
        corrected = value if value.lower().startswith('http') else ''.join(['http://', value])
        scheme, netloc, path = urllib.parse.urlparse(corrected)[:3]

        if not scheme or not netloc or not self._pattern.match(netloc):
            raise ValidationError('{} is not a valid value'.format(value))
        return value


class SizeProperty(PatternProperty):
    _pattern = re.compile('(\d+)x(\d+)', re.IGNORECASE)


class FrequencyCap(PatternProperty):
    _pattern = re.compile('^[1-9]?[0-9]/[1-9]?[0-9][dhmnwy]$')


class DurationPeriod(PatternProperty):
    _pattern = re.compile('^[1-9][0-9]{0,}[wmdy]$')


class AsciiString(PatternProperty):
    _pattern = re.compile('^[a-zA-Z0-9_\- ]+$')


class EBCDICString(PatternProperty):
    _pattern = re.compile('''^[a-zA-Z0-9_\- .(+!$*);|&,%?\/\\\:#@'="\[\]^{}]+$''')


class DigitsString(PatternProperty):
    _pattern = re.compile('^[0-9_\- ]+$')
