#!/usr/bin/env python2.7

from functools import partial

from web.entities.properties import EmailProperty
from web.entities import users


class UserProperty(EmailProperty):

    def configure(self, name):
        super(UserProperty, self).configure(name)
        self.register_prepare_action(lambda v: partial(users.get_user_id_from_email, v))

    def get_entity_value(self, value, context):
        return users.get_user_email_from_id(value)
