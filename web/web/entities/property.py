from inspect import getargspec, ismethod, isfunction


class Property(object):
    _to_model_formatters = None
    _empty_values = None,
    _entity_container_type = None

    def __init__(self, entity_name=None, model_name=None, read_only=False, allow_duplicates=True,
                 entity_container_type=None):
        self._entity_name = entity_name
        self._model_name = model_name
        self._read_only = read_only
        self._entity_container_type = entity_container_type

        self.allow_duplicates = allow_duplicates

        self.prepare_actions = []

    def configure(self, name):
        if self._model_name is None:
            self._model_name = name

        if self._entity_name is None:
            self._entity_name = name

    def _need_wrap(self, func):
        return (not ismethod(func) and not isfunction(func)) or 'context' not in getargspec(func).args

    def action_wrapper(self, func):
        def inner(value, _):
            return func(value)
        return inner

    def wrap_action(self, func):
        if self._need_wrap(func):
            func = self.action_wrapper(func)
        return func

    def register_prepare_action(self, *action):
        for a in action:
            self.prepare_actions.append(self.wrap_action(a))

    def get_model_value(self, value, context):
        if value in self._empty_values:
            return

        if self.prepare_actions:
            for action in self.prepare_actions:
                value = action(value, context)

        return value

    def _get_entity_value(self, value, context):
        return value

    def get_entity_value(self, value, context):
        return self._get_entity_value(value, context)