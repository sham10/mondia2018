
from infra6.infra5.infra4.models import User
from web.entities.validation_exceptions import ValidationError


def get_user_id_from_email(value):
    user = User.get({'email': value})
    if not user:
        raise ValidationError('unknown user {}'.format(value))
    return int(user._id)


def get_user_email_from_id(user_id):
    user = User.get(user_id)
    return user['email']


def get_all():
    users = User.select()
    return users and [{'user_id': u._id,
                       'email': u.email,
                       'name': '%s %s' % (u.first_name, u.last_name)} for u in users] or []
