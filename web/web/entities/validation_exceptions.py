

class ValidationError(Exception):
    pass


class InvalidValueError(ValidationError):
    msg = 'Invalid {0} value: "{1}"'

    def __init__(self, value_type, value, extra_msg=None):
        formatted_msg = self.msg.format(value_type, value)
        if extra_msg:
            formatted_msg = ' - '.join([formatted_msg, extra_msg])
        super(InvalidValueError, self).__init__(formatted_msg)


class InvalidDeferredValueError(ValidationError):
    msg = 'Invalid {0}'

    def __init__(self, value_type, extra_msg=None):
        formatted_msg = self.msg.format(value_type)
        if extra_msg:
            formatted_msg = ' - '.join([formatted_msg, extra_msg])
        super(InvalidDeferredValueError, self).__init__(formatted_msg)


class InvalidDataTypeError(InvalidValueError):
    msg = 'Invalid data type for {0} "{1}"'


class NonUniqueValueError(ValidationError):
    msg = '{0} "{1}" is not unique'

    def __init__(self, value_type, value):
        formatted_msg = self.msg.format(value_type, value)
        super(NonUniqueValueError, self).__init__(formatted_msg)


class MissingRequiredValueError(ValidationError):
    msg = '{} is required and was empty'

    def __init__(self, value_type):
        formatted_msg = self.msg.format(value_type)
        super(MissingRequiredValueError, self).__init__(formatted_msg)


class NotFoundError(Exception):
    pass