import sqlite3
from contextlib import closing

from infra6.infra5.infra4.database.endpoints import get_endpoint_conn


def dict_gen(cursor):
    prev_row_factory = cursor.row_factory
    cursor.row_factory = sqlite3.Row

    try:
        for row in cursor:
            yield dict(row)
    finally:
        cursor.row_factory = prev_row_factory


def default_gen(cursor):
    for row in cursor:
        yield row


def filter(**report_args):
    if not report_args:
        return

    return 'WHERE {}'.format(' AND '.join(['{} = ?'.format(k) for k in report_args.keys()])), list(report_args.values())


def execute_report(report_name, keys=None, as_dict=False, **report_args):
    statement = '''
        SELECT
            {keys}
        FROM
            {table_name}
    '''.format(keys=', '.join(keys) if keys else '*', table_name=report_name)

    execution_args = []

    filter_values = filter(**report_args)
    if filter_values:
        where_clause, execution_args = filter_values
        statement = '{} {}'.format(statement, where_clause)

    reports_db_conn = get_endpoint_conn('scores')
    with closing(reports_db_conn.cursor()) as cursor:
        cursor.execute(statement, execution_args)

        row_gen = dict_gen if as_dict else default_gen
        for row in row_gen(cursor):
            yield row
